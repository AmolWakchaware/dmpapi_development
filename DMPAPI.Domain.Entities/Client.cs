﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.Domain.Entities
{
    public class Client : BaseEntity
    {
        public int Level4Id { get; set; }
        public string Name { get; set; }
        public Level4 Level4 { get; set; }
    }
}
