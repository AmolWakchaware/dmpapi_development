﻿namespace DMPAPI.Domain.Entities
{
    using System.Collections.Generic;
    public class User : BaseEntity
    {        
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsPasswordActivated { get; set; }
        public ICollection<Client> userClients { get; set; }
        public ICollection<UserPageRoleMapping> userPageRoleMappings { get; set; }
    }
}
