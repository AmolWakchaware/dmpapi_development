﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.Domain.Entities
{
   public class Level4 : BaseEntity
    {
        public int Level3Id { get; set; }
        public string Name { get; set; }
        public Level3 Level3 { get; set; }
    }
}
