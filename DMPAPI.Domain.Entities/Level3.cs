﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.Domain.Entities
{
    public class Level3:BaseEntity
    {
        public int Level2Id { get; set; }
        public string Name { get; set; }
        public Level2 Level2 { get; set; }
    }
}
