﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.Domain.Entities
{
    public class Level2 : BaseEntity
    {
        public string Name { get; set; }
        public int Level1Id { get; set; }
        public Level1 Level1 { get; set; }
    }
}
