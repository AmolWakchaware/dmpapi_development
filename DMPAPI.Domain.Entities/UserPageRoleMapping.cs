﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.Domain.Entities
{
    public class UserPageRoleMapping : BaseEntity
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public int PageId { get; set; }
        public bool IsRead { get; set; }
        public bool IsWrite { get; set; }
        public User User { get; set; }
        public Role Role { get; set; }
        public Page Page { get; set; }
    }
}
