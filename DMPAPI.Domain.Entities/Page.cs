﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.Domain.Entities
{
    public class Page : BaseEntity
    {
        public string Name { get; set; }
    }
}
