﻿using DMPAPI.Domain.Entities;
using DMPAPI.Domain.Repositories.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.Domain.Services.Accounts
{
    public class ClientService
    {

        private IUnitOfWork _unitOfWoek;

        public ClientService(IUnitOfWork unitOfWork)
        {
            _unitOfWoek = unitOfWork;
        }

        public Client GetUserClientByID(int userclientID)
        {
            Client userClient = new Client();
            userClient = _unitOfWoek.ClientDetails.GetClientByID(userclientID);
            return userClient;
        }
        public List<Client> GetAllUserClients()
        {
            List<Client> listuserclient = new List<Client>();
            listuserclient = _unitOfWoek.ClientDetails.GetAllClient();
            return listuserclient;
        }

        public Client getclientId(int clientId)
        {
            Client client = new Client();
            client = _unitOfWoek.ClientDetails.GetClientByID(clientId);
            return client;
        }
        public List<Client> getAllclient()
        {
            List<Client> client = new List<Client>();
            client = _unitOfWoek.ClientDetails.GetAllClient();
            return client;
        }
        public bool AddNewclient(Client client)
        {
            try
            {
                _unitOfWoek.ClientDetails.AddClient(client);
                _unitOfWoek.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool UpdateLevel4(Client client)
        {
            try
            {
                _unitOfWoek.ClientDetails.UpdateClient(client);
                _unitOfWoek.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool DeleteClient(Client client)
        {
            try
            {
                _unitOfWoek.ClientDetails.DeleteClient(client);
                _unitOfWoek.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool DeleteClientbyID(int Id)
        {
            try
            {
                _unitOfWoek.ClientDetails.DeleteClientbyID(Id);
                _unitOfWoek.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<Client> GetClientByIDs(string Ids)
        {
            List<Client> client = new List<Client>();
            client = _unitOfWoek.ClientDetails.GetClientByL4IDs(Ids);
            return client;
        }
    }
}
