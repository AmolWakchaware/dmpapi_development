﻿using DMPAPI.Domain.Entities;
using DMPAPI.Domain.Repositories.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.Domain.Services.Accounts
{
    public class PageService
    {

        public IUnitOfWork _unitOfWork;

        public PageService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Page GetPageByID(int pageID)
        {
            try
            {
                Page page = new Page();
                page = _unitOfWork.PageDetails.GetPageByID(pageID);
                return page;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<Page> GetAllPages()
        {
            try
            {
                List<Page> pages = new List<Page>();
                pages = _unitOfWork.PageDetails.GetAllPages();
                return pages;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool AddNewPage(Page page)
        {
            try
            {
                _unitOfWork.PageDetails.AddPage(page);
                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool UpdatePage(Page page)
        {
            try
            {
                _unitOfWork.PageDetails.UpdatePage(page);
                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeletePage(Page page)
        {
            try
            {
                _unitOfWork.PageDetails.DeletePage(page);
                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool DeletePageById(int pageID)
        {
            try
            {
                _unitOfWork.PageDetails.DeletePagebyID(pageID);
                _unitOfWork.SaveChanges();
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
