﻿using DMPAPI.Domain.Entities;
using DMPAPI.Domain.Repositories.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.Domain.Services.Accounts
{
    public class RoleService
    {
        private IUnitOfWork _unitOfWork;

        public RoleService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        public Role getRoleByID(int roleID)
        {
            Role role = new Role();
            role = _unitOfWork.RoleDetails.GetRoleByID(roleID);
            return role;
        }

        public List<Role> getAllRoles()
        {
            List<Role> roles = new List<Role>();
            roles = _unitOfWork.RoleDetails.GetAllRoles();
            return roles;
        }

        public bool AddNewRole(Role role)
        {
            try
            {
                _unitOfWork.RoleDetails.AddRole(role);
                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateRoleDetails(Role role)
        {
            try
            {
                _unitOfWork.RoleDetails.UpdateRole(role);
                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteRole(Role role)
        {
            try
            {
                _unitOfWork.RoleDetails.DeleteRole(role);
                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool DeleteRole(int roleID)
        {
            try
            {
                _unitOfWork.RoleDetails.DeleterolebyID(roleID);
                _unitOfWork.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
