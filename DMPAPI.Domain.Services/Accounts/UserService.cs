﻿using DMPAPI.Domain.Entities;
using DMPAPI.Domain.Repositories.UnitOfWork;
using System;
using System.Collections.Generic;
namespace DMPAPI.Domain.Services.Accounts
{
    public class UserService
    {
        private IUnitOfWork _unitOfWoek;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWoek = unitOfWork;
        }


        public User getUserByID(int userID)
        {
            User user = new User();
            user = _unitOfWoek.UserDetails.GetUserByID(userID);
            return user;
        }

        public List<User> getAllUsers()
        {
            List<User> users = new List<User>();
            users = _unitOfWoek.UserDetails.GetAllUsers();
            return users;
        }

        public bool AddNewUser(User user)
        {
            try
            {
                _unitOfWoek.UserDetails.AddUser(user);
                _unitOfWoek.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool UpdateUserDetails(User user)
        {
            try
            {
                _unitOfWoek.UserDetails.UpdateUser(user);
                _unitOfWoek.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteUser(User user)
        {
            try
            {
                _unitOfWoek.UserDetails.DeleteUser(user);
                _unitOfWoek.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool DeleteUserById(int userID)
        {
            try
            {
                _unitOfWoek.UserDetails.DeleteUserbyID(userID);
                _unitOfWoek.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public User GetUserByCredentials(string username, string password)
        {
            User user = new User();
            user = _unitOfWoek.UserDetails.GetUserCredentails(username, password);
            if (user != null)
            {
                user.Password = string.Empty;
            }
            return user;
        }
    }
}
