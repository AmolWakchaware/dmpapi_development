﻿using AutoMapper;
using DMPAPI.Domain.Entities;
using DMPAPI.Domain.Repositories.UnitOfWork;
using DMPAPI.Domain.Services.Accounts;
using DMPAPI.Models;
using DMPAPI.Validation;
using Microsoft.Web.Http;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web.Http;

namespace DMPAPI.Controllers
{
    [ApiVersion("1.0")]
    [RoutePrefix("api/v{version:apiVersion}/user")]
    [Authorize]
    public class UserController : ApiController
    {
        private IUnitOfWork _unitOfWork;
        UserService userService;
        private IMapper _imapper;
        public UserController(IMapper imapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _imapper = imapper;
            userService = new UserService(_unitOfWork);
            //skillservice = new SkillService(_unitOfWork);
            //demandService = new DemandService(_unitOfWork);
        }

        [Route("Get")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<User>))]
        // [SwaggerResponseExample(HttpStatusCode.OK, typeof(TestsExample))]
        public IHttpActionResult GetAllUsers()
        {

            //  var userid = (ClaimsIdentity)User.UserId;
            var test = userService.getAllUsers();
            List<UserSummary> lstSum = new List<UserSummary>();
            foreach (var item in test)
            {
                lstSum.Add(_imapper.Map<UserSummary>(item));
            }

            return Ok(lstSum);
        }

        [Route("GetById")]
        public IHttpActionResult GetUserById(int id)
        {
            return Ok(userService.getUserByID(id));
        }

        [HttpPost]
        [Route("Post")]
        public IHttpActionResult CreateUser(VM_User saveuser)
        {
            try
            {
                var _uservalidation = new UserValidation(_imapper, _unitOfWork);
                var _validationMsg = _uservalidation.ValidateSave(saveuser);
                if (_validationMsg.Count > 0)
                {
                    return Content(HttpStatusCode.BadRequest, _validationMsg);
                }
                else
                {
                    var _user = _uservalidation?.user;
                    if (_user != null)
                    {
                        var identity = (ClaimsIdentity)User.Identity;
                        var uid = identity.FindFirst("UserID").Value;
                        _user.UserId = Convert.ToInt32(identity.FindFirst("UserID").Value);
                        userService.AddNewUser(_user);
                        return Ok();
                    }
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // GET: Users/Edit/5
        //[Route("Put")]
        //public IHttpActionResult Edit(int id)
        //{
        //    User user = userService.getUserByID(id);
        //    return Ok(user);
        //}

        // POST: Users/Edit/5
        [HttpPut]
        [Route("Put")]
        //public ActionResult Edit(int id, FormCollection collection)
        public IHttpActionResult UpdateUser(VM_User vM_User)
        {
            try
            {
                var _uservalidation = new UserValidation(_imapper, _unitOfWork);
                var _validationMsg = _uservalidation.ValidateSave(vM_User);
                if (_validationMsg.Count > 0)
                {
                    return Content(HttpStatusCode.BadRequest, _validationMsg);
                }
                else
                {
                    var _user = _uservalidation?.user;
                    if (_user != null)
                    {
                        var userid = (ClaimsIdentity)User.Identity;
                        _user.UserId = 1;//From session
                        userService.UpdateUserDetails(_user);
                        return Ok();
                    }
                    return BadRequest();
                }
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        //// GET: Users/Delete/5
        //public IHttpActionResult Delete(int id)
        //{
        //    User user = userService.getUserByID(id);
        //    return Ok(user);
        //}

        // POST: Users/Delete/5
        [HttpDelete]
        [Route("Delete/{id}")]
        public IHttpActionResult DeleteUser(int id)
        {
            try
            {
                // TODO: Add delete logic here
                userService.DeleteUserById(id);
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [Route("ChangePassword")]
        public IHttpActionResult ChangePassword(string opwd, string npwd)
        {
            try
            {

                return Ok();
            }
            catch (Exception ex)
            {

                return BadRequest();
            }

        }

    }
}
