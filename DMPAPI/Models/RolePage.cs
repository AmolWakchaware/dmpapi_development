﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DMPAPI.Models
{
    public class RolePage
    {
        [Required]
        public int RoleId { get; set; }
        [Required]
        public int PageId { get; set; }
        [Required]
        public bool IsRead { get; set; }
        [Required]
        public bool IsWrite { get; set; }
    }
}