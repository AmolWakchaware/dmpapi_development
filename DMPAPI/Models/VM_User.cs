﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DMPAPI.Models
{
    public class VM_User
    {
        //[Required]
        //public int? UserId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        //public string Password { get; set; }
        public int?[] userClients { get; set; }
        public List<RolePage> RolePage { get; set; }
    }
}