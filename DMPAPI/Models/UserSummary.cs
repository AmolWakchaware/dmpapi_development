﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DMPAPI.Models
{
    public class UserSummary
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public List<Client> UserClients { get; set; }
        public List<RolePage> UserRolePages { get; set; }

        public class Client
        {
            public string ClientId { get; set; }
            public string Level4Id { get; set; }
            public string Level3Id { get; set; }
            public string Level2Id { get; set; }
            public string Level1Id { get; set; }

            public string ClientName { get; set; }
            public string Level4Name { get; set; }
            public string Level3Name { get; set; }
            public string Level2Name { get; set; }
            public string Level1Name { get; set; }
        }
    }
}