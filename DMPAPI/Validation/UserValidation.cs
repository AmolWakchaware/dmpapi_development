﻿using AutoMapper;
using DMPAPI.Domain.Entities;
using DMPAPI.Domain.Repositories.UnitOfWork;
using DMPAPI.Domain.Services.Accounts;
using DMPAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DMPAPI.Validation
{
    public class UserValidation
    {

        private IUnitOfWork _unitOfWork;
        UserService userservice;
        PageService pageservice;
        RoleService roleService;
        ClientService userClientService;
        private IMapper _imapper;
        public User user { get; set; }

        public UserValidation(IMapper imapper, IUnitOfWork unitOfWork)
        {
            _imapper = imapper;
            _unitOfWork = unitOfWork;
            userservice = new UserService(_unitOfWork);
            userClientService = new ClientService(_unitOfWork);
            pageservice = new PageService(_unitOfWork);
            roleService = new RoleService(_unitOfWork);
        }

        public List<string> ValidateSave(VM_User saveuser)
        {
            var validationMsg = new List<string>();
            var invalidIds = new List<int?>();
            user = _imapper.Map<User>(saveuser);
            user.userPageRoleMappings = new List<UserPageRoleMapping>();
            user.userClients = new List<Client>();
            UserPageRoleMapping userPageRoleMapping = new UserPageRoleMapping();
            var _userClients = userClientService.GetAllUserClients();
            foreach (var userclientId in saveuser.userClients)
            {
                var userclnt = _userClients.Find(x => x.Id == userclientId);
                if (userclnt == null)
                {
                    invalidIds.Add(userclientId);
                }
                else
                {
                    user.userClients.Add(userclnt);
                }
            }
            if (invalidIds.Any())
                validationMsg.Add(string.Concat($"{nameof(saveuser.userClients)} contains invalid ids ", string.Join(",", invalidIds)));

            invalidIds.Clear();
            var _pages = pageservice.GetAllPages();
            var _erole = roleService.getAllRoles();
            foreach (var item in saveuser.RolePage)
            {
                var pageno = _pages.Find(x => x.Id == item.PageId);
                var existingrole = _erole.Find(x => x.Id == item.RoleId);
                if (pageno == null)
                {
                    invalidIds.Add(item.PageId);
                }
                else
                {
                    userPageRoleMapping.PageId = Convert.ToInt32(pageno.Id);
                }
                if (existingrole == null)
                {
                    invalidIds.Add(item.RoleId);
                }
                else
                {
                    userPageRoleMapping.RoleId = Convert.ToInt32(existingrole.Id);
                }
                if (userPageRoleMapping.IsRead != null)
                {
                    userPageRoleMapping.IsRead = item.IsRead;
                }
                if (userPageRoleMapping.IsWrite != null)
                {
                    userPageRoleMapping.IsWrite = item.IsWrite;
                }
                if (invalidIds.Any())
                    validationMsg.Add(string.Concat($"{nameof(item.RoleId)} contains invalid ids ", string.Join(",", invalidIds)));

                user.userPageRoleMappings.Add(userPageRoleMapping);
            }

            return validationMsg;
        }
    }
}