﻿using AutoMapper;
using Entities = DMPAPI.Domain.Entities;

namespace DMPAPI.DB.Infrastructure.Repositories
{
    public class EntityMapper<TSource> : Profile where TSource : class
    {
        private object resultEntity;


        public EntityMapper()
        {
            CreateMap<Entities.User, DataModel.USER>()
                .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
                .ForMember(x => x.userClients, op => op.Ignore())
                .ForMember(x => x.userPageRoleMappings, op => op.Ignore())
                .ForMember(x => x.CreatedDate, op => op.Ignore())
                .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
                .ForMember(x => x.ModifiedDate, op => op.Ignore())
                .ForMember(x => x.IsActive, op => op.Ignore());

            CreateMap<DataModel.USER, Entities.User>()
                .ForMember(x => x.Password, op => op.MapFrom(x => x.Password))
                .ForMember(x => x.userPageRoleMappings, op => op.MapFrom(x => x.userPageRoleMappings))
                .ForMember(x => x.userClients, op => op.MapFrom(x => x.userClients))
                // .ForMember(x => x.IsActive, op => op.MapFrom(x => x.IsActive))
                .ForMember(x => x.UserId, op => op.Ignore());

            //CreateMap<Entities.Location, DataModel.Location>()
            //    .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
            //    .ForMember(x => x.CreatedDate, op => op.Ignore())
            //    .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
            //    .ForMember(x => x.ModifiedDate, op => op.Ignore())
            //    .ForMember(x => x.IsActive, op => op.Ignore());

            //CreateMap<DataModel.Location, Entities.Location>()
            //   .ForMember(x => x.UserId, op => op.Ignore());

            //CreateMap<DataModel.Status, Entities.Status>()
            //   .ForMember(x => x.UserId, op => op.Ignore());


            //CreateMap<Entities.Project, DataModel.Project>()
            //    .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
            //    .ForMember(x => x.CreatedDate, op => op.Ignore())
            //    .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
            //    .ForMember(x => x.ModifiedDate, op => op.Ignore())
            //    .ForMember(x => x.IsActive, op => op.Ignore());
            //CreateMap<DataModel.Project, Entities.Project>()
            //   .ForMember(x => x.UserId, op => op.Ignore());

            //CreateMap<Entities.Panel, DataModel.Panel>()
            //    .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
            //    .ForMember(x => x.CreatedDate, op => op.Ignore())
            //    .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
            //    .ForMember(x => x.ModifiedDate, op => op.Ignore())
            //    .ForMember(x => x.IsActive, op => op.Ignore());

            //CreateMap<DataModel.Panel, Entities.Panel>()
            //   .ForMember(x => x.UserId, op => op.Ignore());

            //CreateMap<Entities.Skill, DataModel.Skill>()
            //    .ForMember(x => x.CreatedDate, op => op.Ignore())
            //    .ForMember(x => x.ModifiedDate, op => op.Ignore())
            //    .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
            //    .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
            //   .ForMember(x => x.IsActive, op => op.Ignore());
            //CreateMap<DataModel.Skill, Entities.Skill>()
            //    .ForMember(x => x.UserId, op => op.Ignore());

            //CreateMap<Entities.OpCo, DataModel.OpCo>()
            //    .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
            //    .ForMember(x => x.CreatedDate, op => op.Ignore())
            //    .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
            //    .ForMember(x => x.ModifiedDate, op => op.Ignore())
            //    .ForMember(x => x.IsActive, op => op.Ignore());

            //CreateMap<DataModel.OpCo, Entities.OpCo>()
            //   .ForMember(x => x.UserId, op => op.Ignore());

            //CreateMap<Entities.Demand, DataModel.Demand>()
            // .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
            // .ForMember(x => x.CreatedDate, op => op.Ignore())
            // .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
            // .ForMember(x => x.ModifiedDate, op => op.Ignore())
            // .ForMember(x => x.IsActive, op => op.Ignore())
            // .ForMember(x => x.DemandNumber, op => op.Ignore());

            //CreateMap<DataModel.Demand, Entities.Demand>()
            //    //.ForMember(x => x.PrimarySkill, op => op.MapFrom(x => x.PrimarySkill))
            //    //.ForMember(x => x.SecondarySkill, op => op.MapFrom(x => x.SecondarySkill))
            //    //.ForMember(x => x.Location, op => op.MapFrom(x => x.Location))
            //    //.ForMember(x => x.Status, op => op.MapFrom(x => x.Status))
            //    .ForMember(x => x.UserId, op => op.Ignore());

            //CreateMap<Entities.Resource, DataModel.Resource>()
            //    .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
            //    .ForMember(x => x.CreatedDate, op => op.Ignore())
            //    .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
            //    .ForMember(x => x.ModifiedDate, op => op.Ignore())
            //    .ForMember(x => x.IsActive, op => op.Ignore());

            //CreateMap<DataModel.Resource, Entities.Resource>()
            //    .ForMember(x => x.PrimarySkill, op => op.Ignore())
            //    .ForMember(x => x.SecondarySkill, op => op.Ignore())
            //    .ForMember(x => x.UserId, op => op.Ignore());


            //CreateMap<Entities.InterviewerType, InterviewerType>()
            //    .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
            //    .ForMember(x => x.CreatedDate, op => op.Ignore())
            //    .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
            //    .ForMember(x => x.ModifiedDate, op => op.Ignore())
            //    .ForMember(x => x.IsActive, op => op.Ignore());

            //CreateMap<DataModel.InterviewerType, Entities.InterviewerType>()
            //   .ForMember(x => x.UserId, op => op.Ignore());

            //CreateMap<Entities.Country, DataModel.Country>()
            //   .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
            //   .ForMember(x => x.CreatedDate, op => op.Ignore())
            //   .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
            //   .ForMember(x => x.ModifiedDate, op => op.Ignore())
            //   .ForMember(x => x.IsActive, op => op.Ignore());

            //CreateMap<DataModel.Country, Entities.Country>()
            //   .ForMember(x => x.UserId, op => op.Ignore());

            CreateMap<Entities.Role, DataModel.Role>()
             .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
             .ForMember(x => x.CreatedDate, op => op.Ignore())
             .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
             .ForMember(x => x.ModifiedDate, op => op.Ignore())
             .ForMember(x => x.IsActive, op => op.Ignore());

            CreateMap<DataModel.Role, Entities.Role>()
               .ForMember(x => x.UserId, op => op.Ignore());

            CreateMap<Entities.Page, DataModel.Page>()
             .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
             .ForMember(x => x.CreatedDate, op => op.Ignore())
             .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
             .ForMember(x => x.ModifiedDate, op => op.Ignore());

            CreateMap<DataModel.Page, Entities.Page>()
               .ForMember(x => x.UserId, op => op.Ignore());

            CreateMap<Entities.Level1, DataModel.Level1>()
              .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
              .ForMember(x => x.CreatedDate, op => op.Ignore())
              .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
              .ForMember(x => x.ModifiedDate, op => op.Ignore())
              .ForMember(x => x.IsActive, op => op.Ignore());

            CreateMap<DataModel.Level1, Entities.Level1>()
               .ForMember(x => x.UserId, op => op.Ignore());

            CreateMap<Entities.Level2, DataModel.Level2>()
             .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
             .ForMember(x => x.CreatedDate, op => op.Ignore())
             .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
             .ForMember(x => x.ModifiedDate, op => op.Ignore())
             .ForMember(x => x.IsActive, op => op.Ignore());

            CreateMap<DataModel.Level2, Entities.Level2>()
               .ForMember(x => x.UserId, op => op.Ignore());

            CreateMap<Entities.Level3, DataModel.Level3>()
            .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
            .ForMember(x => x.CreatedDate, op => op.Ignore())
            .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
            .ForMember(x => x.ModifiedDate, op => op.Ignore())
            .ForMember(x => x.IsActive, op => op.Ignore());

            CreateMap<DataModel.Level3, Entities.Level3>()
               .ForMember(x => x.UserId, op => op.Ignore());

            CreateMap<Entities.Level4, DataModel.Level4>()
            .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
            .ForMember(x => x.CreatedDate, op => op.Ignore())
            .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
            .ForMember(x => x.ModifiedDate, op => op.Ignore())
            .ForMember(x => x.IsActive, op => op.Ignore());

            CreateMap<DataModel.Level4, Entities.Level4>()
               .ForMember(x => x.UserId, op => op.Ignore());

            CreateMap<Entities.Client, DataModel.Client>()
           .ForMember(x => x.CreatedBy, op => op.MapFrom(x => x.UserId))
           .ForMember(x => x.CreatedDate, op => op.Ignore())
           .ForMember(x => x.ModifiedBy, op => op.MapFrom(x => x.UserId))
           .ForMember(x => x.ModifiedDate, op => op.Ignore())
           .ForMember(x => x.IsActive, op => op.Ignore());

            CreateMap<DataModel.Client, Entities.Client>()
               .ForMember(x => x.UserId, op => op.Ignore());


            CreateMap<DataModel.UserPageRoleMapping, Entities.UserPageRoleMapping>()
              .ForMember(x => x.UserId, op => op.Ignore());
        }
        public object getMappedObject(TSource source)
        {
            if (typeof(TSource) == typeof(Entities.User))
            {
                resultEntity = Mapper.Map<DataModel.USER>(source);
            }
            else if (typeof(TSource) == typeof(DataModel.USER))
            {
                resultEntity = Mapper.Map<Entities.User>(source);
            }
            //else if (typeof(TSource) == typeof(Entities.Location))
            //{
            //    resultEntity = Mapper.Map<DataModel.Location>(source);
            //}
            //else if (typeof(TSource) == typeof(DataModel.Location))
            //{
            //    resultEntity = Mapper.Map<Entities.Location>(source);
            //}
            //else if (typeof(TSource) == typeof(Entities.Skill))
            //{
            //    resultEntity = Mapper.Map<DataModel.Skill>(source);
            //}
            //else if (typeof(TSource) == typeof(DataModel.Skill))
            //{
            //    resultEntity = Mapper.Map<Entities.Skill>(source);
            //}
            //else if (typeof(TSource) == typeof(Entities.Project))
            //{
            //    resultEntity = Mapper.Map<DataModel.Project>(source);
            //}
            //else if (typeof(TSource) == typeof(DataModel.Project))
            //{
            //    resultEntity = Mapper.Map<Entities.Project>(source);
            //}
            //else if (typeof(TSource) == typeof(Entities.Panel))
            //{
            //    resultEntity = Mapper.Map<DataModel.Panel>(source);
            //}
            //else if (typeof(TSource) == typeof(DataModel.Panel))
            //{
            //    resultEntity = Mapper.Map<Entities.Panel>(source);
            //}
            //else if (typeof(TSource) == typeof(Entities.OpCo))
            //{
            //    resultEntity = Mapper.Map<DataModel.OpCo>(source);
            //}
            //else if (typeof(TSource) == typeof(DataModel.OpCo))
            //{
            //    resultEntity = Mapper.Map<Entities.OpCo>(source);
            //}
            //else if (typeof(TSource) == typeof(Entities.Resource))
            //{
            //    resultEntity = Mapper.Map<DataModel.Resource>(source);
            //}
            //else if (typeof(TSource) == typeof(DataModel.Resource))
            //{
            //    resultEntity = Mapper.Map<Entities.Resource>(source);
            //}
            //else if (typeof(TSource) == typeof(Entities.InterviewerType))
            //{
            //    resultEntity = Mapper.Map<DataModel.InterviewerType>(source);
            //}
            //else if (typeof(TSource) == typeof(DataModel.InterviewerType))
            //{
            //    resultEntity = Mapper.Map<Entities.InterviewerType>(source);
            //}
            //else if (typeof(TSource) == typeof(Entities.Country))
            //{
            //    resultEntity = Mapper.Map<DataModel.Country>(source);
            //}
            //else if (typeof(TSource) == typeof(DataModel.Country))
            //{
            //    resultEntity = Mapper.Map<Entities.Country>(source);
            //}
            //else if (typeof(TSource) == typeof(Entities.Demand))
            //{
            //    resultEntity = Mapper.Map<DataModel.Demand>(source);
            //}
            //else if (typeof(TSource) == typeof(DataModel.Demand))
            //{
            //    resultEntity = Mapper.Map<Entities.Demand>(source);
            //}
            else if (typeof(TSource) == typeof(Entities.Role))
            {
                resultEntity = Mapper.Map<DataModel.Role>(source);
            }
            else if (typeof(TSource) == typeof(DataModel.Role))
            {
                resultEntity = Mapper.Map<Entities.Role>(source);
            }
            else if (typeof(TSource) == typeof(Entities.Page))
            {
                resultEntity = Mapper.Map<DataModel.Page>(source);
            }
            else if (typeof(TSource) == typeof(DataModel.Page))
            {
                resultEntity = Mapper.Map<Entities.Page>(source);
            }
            else if (typeof(TSource) == typeof(Entities.Level1))
            {
                resultEntity = Mapper.Map<DataModel.Level1>(source);
            }
            else if (typeof(TSource) == typeof(DataModel.Level1))
            {
                resultEntity = Mapper.Map<Entities.Level1>(source);
            }
            else if (typeof(TSource) == typeof(Entities.Level2))
            {
                resultEntity = Mapper.Map<DataModel.Level2>(source);
            }
            else if (typeof(TSource) == typeof(DataModel.Level2))
            {
                resultEntity = Mapper.Map<Entities.Level2>(source);
            }
            else if (typeof(TSource) == typeof(Entities.Level3))
            {
                resultEntity = Mapper.Map<DataModel.Level3>(source);
            }
            else if (typeof(TSource) == typeof(DataModel.Level3))
            {
                resultEntity = Mapper.Map<Entities.Level3>(source);
            }
            else if (typeof(TSource) == typeof(Entities.Level4))
            {
                resultEntity = Mapper.Map<DataModel.Level4>(source);
            }
            else if (typeof(TSource) == typeof(DataModel.Level4))
            {
                resultEntity = Mapper.Map<Entities.Level4>(source);
            }
            else if (typeof(TSource) == typeof(Entities.Client))
            {
                resultEntity = Mapper.Map<DataModel.Client>(source);
            }
            else if (typeof(TSource) == typeof(DataModel.Client))
            {
                resultEntity = Mapper.Map<Entities.Client>(source);
            }
            else if (typeof(TSource) == typeof(Entities.UserPageRoleMapping))
            {
                resultEntity = Mapper.Map<DataModel.UserPageRoleMapping>(source);
            }
            else if (typeof(TSource) == typeof(DataModel.UserPageRoleMapping))
            {
                resultEntity = Mapper.Map<Entities.UserPageRoleMapping>(source);
            }
            return resultEntity;
        }
    }
}
