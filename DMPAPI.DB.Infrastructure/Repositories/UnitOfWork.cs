﻿namespace DMPAPI.DB.Infrastructure.Repositories
{
    using System;
    using DMPAPI.Domain.Repositories.UnitOfWork;
    using DMPAPI.Domain.Repositories.Accounts;
    using DMPAPI.DB.Infrastructure.Repositories.Accounts;
    using System.Data.Entity.Validation;

    public class UnitOfWork : IUnitOfWork
    {
        private DataModel.DMPDbContext dbContext;
        public IUserRepository UserDetails { get; private set; }
        //public ILocationRepository LocationDetails { get; private set; }
        //public ISkillsRepository SkillDetails { get; private set; }
        //public IProjectRepository ProjectDetails { get; private set; }
        //public IPanelRepository PanelDetails { get; private set; }
        //public IOpCoRepository OpCoDetails { get; private set; }
        //public IResourceRepository ResourceDetails { get; private set; }
        //public IInterviewerTypeRepository InterviewerTypeDetails { get; private set; }
        //public ICountryRepository CountryDetails { get; private set; }
        //public IDemandRepository DemandDetails { get; private set; }
        public IRoleRepository RoleDetails { get; private set; }
        public IPageRepository PageDetails { get; private set; }
        //public ILevel1Repository Level1Details { get; private set; }
        //public ILevel2Repository Level2Details { get; private set; }
        //public ILevel3Repository Level3Details { get; private set; }
        //public ILevel4Repository Level4Details { get; private set; }
        public IClientRepository ClientDetails { get; set; }
        public UnitOfWork()
        {
            dbContext = new DataModel.DMPDbContext();
            UserDetails = new UserRepository(dbContext);
            //LocationDetails = new LocationRepository(dbContext);
            //SkillDetails = new SkillRepository(dbContext);
            //ProjectDetails = new ProjectRepository(dbContext);
            //PanelDetails = new PanelRepository(dbContext);
            //OpCoDetails = new OpCoRepository(dbContext);
            //ResourceDetails = new ResourceRepository(dbContext);
            //InterviewerTypeDetails = new InterviewerTypeRepository(dbContext);
            //CountryDetails = new CountryRepository(dbContext);
            //DemandDetails = new DemandRepository(dbContext);
            //RoleDetails = new RoleRepository(dbContext);
            //PageDetails = new PageRepository(dbContext);
            //Level1Details = new Level1Repository(dbContext);
            //Level2Details = new Level2Repository(dbContext);
            //Level3Details = new Level3Repository(dbContext);
            //Level4Details = new Level4Repository(dbContext);
            //ClientDetails = new ClientRepository(dbContext);
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        public int RollbackChanges()
        {
            throw new NotImplementedException();
        }

        public int SaveChanges()
        {
            try
            {
                return dbContext.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        // ve.PropertyName
                        // ve.ErrorMessage);
                    }
                }

                throw;
            }
        }
    }
}
