﻿using DMPAPI.DB.Infrastructure.DataModel;
using DMPAPI.Domain.Repositories.Accounts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Entities = DMPAPI.Domain.Entities;

namespace DMPAPI.DB.Infrastructure.Repositories.Accounts
{
    public class ClientRepository : Repository<Entities.Client>, IClientRepository
    {
        private EntityMapper<Entities.Client> mapperFromDomain = new EntityMapper<Entities.Client>();
        private EntityMapper<DataModel.Client> mapperFromDb = new EntityMapper<DataModel.Client>();

        public ClientRepository(DbContext context) : base(context)
        {
        }
        private DMPDbContext dbContext { get { return _dbContext as DMPDbContext; } }

        public int AddClient(Entities.Client client)
        {
            DataModel.Client ClientEntity = (DataModel.Client)mapperFromDomain.getMappedObject(client);
            ClientEntity.CreatedDate = ClientEntity.ModifiedDate = DateTime.Now;
            ClientEntity.IsActive = true;
            _dbContext.Set<DataModel.Client>().Add(ClientEntity);
            return 0;
        }

        public int DeleteClient(Entities.Client client)
        {
            DataModel.Client ClientEntity = dbContext.Client.Where(x => x.Id == client.Id).FirstOrDefault();
            if (ClientEntity != null)
            {
                dbContext.Client.Remove(ClientEntity);

            }
            return 0;
        }

        public List<Entities.Client> GetAllClient()
        {
            List<DataModel.Client> listClient = dbContext.Client.ToList();
            List<Entities.Client> listclient = new List<Entities.Client>();

            foreach (DataModel.Client ClientEntity in listClient)
            {
                listclient.Add((Entities.Client)mapperFromDb.getMappedObject(ClientEntity));
            }

            return listclient;
        }

        public Entities.Client GetClientByID(int Id)
        {
            DataModel.Client ClientEntity = dbContext.Client.Where(X => X.Id == Id).SingleOrDefault();
            Entities.Client domainClient = (Entities.Client)mapperFromDb.getMappedObject(ClientEntity);
            return domainClient;
        }

        public int UpdateClient(Entities.Client Client)
        {
            DataModel.Client ClientEntity = dbContext.Client.Where(X => X.Id == Client.Id).SingleOrDefault();
            if (ClientEntity != null)
            {
                DataModel.Client ClientUpdated = (DataModel.Client)mapperFromDomain.getMappedObject(Client);
                ClientUpdated.ModifiedDate = DateTime.Now;
                ClientUpdated.CreatedDate = ClientUpdated.CreatedDate;
                ClientUpdated.IsActive = ClientUpdated.IsActive;
                dbContext.Entry(ClientUpdated).CurrentValues.SetValues(ClientUpdated);
                dbContext.Entry(ClientUpdated).State = EntityState.Modified;
            }
            return 0;
        }

        public int DeleteClientbyID(int Id)
        {
            DataModel.Client ClientEntity = dbContext.Client.Where(X => X.Id == Id).SingleOrDefault();
            ClientEntity.ModifiedDate = DateTime.Now;
            ClientEntity.IsActive = false;
            dbContext.Entry(ClientEntity).CurrentValues.SetValues(ClientEntity);
            return 0;
        }
        public List<Entities.Client> GetClientByL4IDs(string Ids)
        {
            List<int> idList = new List<int>();
            string[] arrIds = Ids.Split(new char[] { ',' });
            for (var i = 0; i < arrIds.Length; i++)
            {
                idList.Add(Int32.Parse(arrIds[i]));
            }
            ArrayList arrList = new ArrayList();
            var listClient = dbContext.Client.Where(X => idList.Contains(X.Level4Id)).ToList();
            List<Entities.Client> listclient = new List<Entities.Client>();
            foreach (DataModel.Client ClientEntity in listClient)
            {
                listclient.Add((Entities.Client)mapperFromDb.getMappedObject(ClientEntity));
            }
            return listclient;
        }
    }
}
