﻿using DMPAPI.DB.Infrastructure.DataModel;
using DMPAPI.Domain.Entities;
using DMPAPI.Domain.Repositories.Accounts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
namespace DMPAPI.DB.Infrastructure.Repositories.Accounts
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private EntityMapper<User> mapperFromDomain = new EntityMapper<User>();
        private EntityMapper<USER> mapperFromDb = new EntityMapper<USER>();

        public UserRepository(DbContext context) : base(context)
        {
        }
        private DMPDbContext dbContext { get { return _dbContext as DMPDbContext; } }

        public int AddUser(User user)
        {
            USER userEntity = (USER)mapperFromDomain.getMappedObject(user);
            userEntity.userClients = new List<DataModel.Client>();
            List<DataModel.UserPageRoleMapping> lstuserPageRoleMapping = new List<DataModel.UserPageRoleMapping>();
            DataModel.UserPageRoleMapping userPageRoleMapping = new DataModel.UserPageRoleMapping();
            var _clients = dbContext.Client.ToList();
            foreach (Domain.Entities.Client clnt in user.userClients)
            {
                userEntity.userClients.Add(_clients.Find(x => x.Id == clnt.Id));
            }
            userEntity.Password = GeneratePassword();
            userEntity.CreatedDate = userEntity.ModifiedDate = DateTime.Now;
            userEntity.IsActive = true;
            userEntity.IsPasswordActivated = false;
            foreach (var itm in user.userPageRoleMappings)
            {
                userPageRoleMapping.RoleId = itm.RoleId;
                userPageRoleMapping.PageId = itm.PageId;
                userPageRoleMapping.IsRead = itm.IsRead;
                userPageRoleMapping.IsWrite = itm.IsWrite;
                lstuserPageRoleMapping.Add(userPageRoleMapping);
            }
            userEntity.userPageRoleMappings = lstuserPageRoleMapping;
            userEntity.CreatedDate = userEntity.ModifiedDate = DateTime.Now;
            _dbContext.Set<USER>().Add(userEntity);
            if (userEntity.Email != null || userEntity.Email != "")
            {
                SMail.SendMail(userEntity.Email, "DMP New User Credentials", "Your Login password is :<strong> " + userEntity.Password + "</strong>. Request you to change the password.");
            }
            return 0;
        }

        public int DeleteUser(User user)
        {
            USER userEntity = dbContext.USERS.Where(x => x.Id == user.UserId).FirstOrDefault();
            if (userEntity != null)
            {
                dbContext.USERS.Remove(userEntity);
            }
            return 0;
        }

        public List<User> GetAllUsers()
        {
            List<USER> listUSER = dbContext.USERS.ToList();
            List<User> listUser = new List<User>();

            foreach (USER userEntity in listUSER)
            {
                listUser.Add((User)mapperFromDb.getMappedObject(userEntity));
            }

            return listUser;
        }

        public User GetUserByID(int Id)
        {
            USER userEntity = dbContext.USERS.Where(X => X.Id == Id && X.IsActive).SingleOrDefault();
            User domainUser = (User)mapperFromDb.getMappedObject(userEntity);
            return domainUser;
        }

        public int UpdateUser(User user)
        {
            DataModel.USER userEntity = dbContext.USERS.Where(X => X.Id == user.UserId).SingleOrDefault();
            if (userEntity != null)
            {
                DataModel.USER userUpdated = (DataModel.USER)mapperFromDomain.getMappedObject(user);
                List<DataModel.UserPageRoleMapping> lstuserPageRoleMapping = new List<DataModel.UserPageRoleMapping>();
                DataModel.UserPageRoleMapping userPageRoleMapping = new DataModel.UserPageRoleMapping();
                userUpdated.ModifiedDate = DateTime.Now;
                userUpdated.CreatedDate = userEntity.CreatedDate;
                userUpdated.IsActive = userUpdated.IsActive;
                userEntity.userPageRoleMappings = new List<DataModel.UserPageRoleMapping>();
                foreach (var itm in user.userPageRoleMappings)
                {
                    userPageRoleMapping.RoleId = itm.RoleId;
                    userPageRoleMapping.PageId = itm.PageId;
                    userPageRoleMapping.IsRead = itm.IsRead;
                    userPageRoleMapping.IsWrite = itm.IsWrite;
                    lstuserPageRoleMapping.Add(userPageRoleMapping);
                }
                userEntity.userPageRoleMappings = lstuserPageRoleMapping;
                _dbContext.Entry(userEntity).CurrentValues.SetValues(userUpdated);
                _dbContext.Entry(userEntity).State = EntityState.Modified;
            }
            return 0;
        }

        public int DeleteUserbyID(int Id)
        {
            USER userEntity = dbContext.USERS.Where(X => X.Id == Id).SingleOrDefault();
            userEntity.ModifiedDate = DateTime.Now;
            userEntity.IsActive = false;
            dbContext.Entry(userEntity).CurrentValues.SetValues(userEntity);
            return 0;
        }


        public User GetUserCredentails(string username, string password)
        {
            USER userEntity = dbContext.USERS.Where(m => m.Name == username && m.Password == password).FirstOrDefault();
            User domainUser = (User)mapperFromDb.getMappedObject(userEntity);
            return domainUser;
        }


        public static string GeneratePassword()
        {
            int PasswordLength = 8;
            string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }

        //public string ChangePassword(string opwd, string npwd)
        //{
        //    try
        //    {
        //        var _users = dbContext.USERS.ToList();
        //        //   var _users= _dbContext.Set(User).
        //        //USER userEntity = dbContext.USERS.Where(X => X.Id == Id && X.IsActive).SingleOrDefault();
        //        return "";
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //}

    }
}
