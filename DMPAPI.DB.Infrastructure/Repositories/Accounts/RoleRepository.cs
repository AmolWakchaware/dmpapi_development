﻿using DMPAPI.DB.Infrastructure.DataModel;
using DMPAPI.Domain.Repositories.Accounts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Entities = DMPAPI.Domain.Entities;
namespace DMPAPI.DB.Infrastructure.Repositories.Accounts
{
    public class RoleRepository : Repository<Entities.Role>, IRoleRepository
    {
        private EntityMapper<Entities.Role> mapperFromDomain = new EntityMapper<Entities.Role>();
        private EntityMapper<DataModel.Role> mapperFromDb = new EntityMapper<DataModel.Role>();

        public RoleRepository(DbContext context) : base(context)
        {
        }
        private DMPDbContext dbContext { get { return _dbContext as DMPDbContext; } }

        public int AddRole(Entities.Role role)
        {
            DataModel.Role RoleEntity = (DataModel.Role)mapperFromDomain.getMappedObject(role);
            RoleEntity.CreatedDate = RoleEntity.ModifiedDate = DateTime.Now;
            _dbContext.Set<DataModel.Role>().Add(RoleEntity);
            return 0;
        }

        public int DeleteRole(Entities.Role role)
        {
            DataModel.Role RoleEntity = dbContext.Role.Where(x => x.Id == role.Id).FirstOrDefault();
            if (RoleEntity != null)
            {
                dbContext.Role.Remove(RoleEntity);
            }
            return 0;
        }

        public List<Entities.Role> GetAllRoles()
        {
            List<DataModel.Role> listOPCO = dbContext.Role.ToList();
            List<Entities.Role> listRole = new List<Entities.Role>();

            foreach (DataModel.Role RoleEntity in listOPCO)
            {
                listRole.Add((Entities.Role)mapperFromDb.getMappedObject(RoleEntity));
            }

            return listRole;
        }

        public Entities.Role GetRoleByID(int Id)
        {
            DataModel.Role RoleEntity = dbContext.Role.Where(X => X.Id == Id).SingleOrDefault();
            Entities.Role domainRole = (Entities.Role)mapperFromDb.getMappedObject(RoleEntity);
            return domainRole;
        }

        public int UpdateRole(Entities.Role role)
        {
            DataModel.Role RoleEntity = dbContext.Role.Where(X => X.Id == role.Id).SingleOrDefault();
            if (RoleEntity != null)
            {
                DataModel.Role RoleUpdated = (DataModel.Role)mapperFromDomain.getMappedObject(role);
                RoleUpdated.ModifiedDate = DateTime.Now;
                RoleUpdated.CreatedDate = RoleUpdated.CreatedDate;
                dbContext.Entry(RoleEntity).CurrentValues.SetValues(RoleUpdated);
                dbContext.Entry(RoleEntity).State = EntityState.Modified;
            }
            return 0;
        }

        public int DeleterolebyID(int Id)
        {
            DataModel.Role RoleEntity = dbContext.Role.Where(X => X.Id == Id).SingleOrDefault();
            RoleEntity.ModifiedDate = DateTime.Now;
            dbContext.Entry(RoleEntity).CurrentValues.SetValues(RoleEntity);
            return 0;
        }

    }
}
