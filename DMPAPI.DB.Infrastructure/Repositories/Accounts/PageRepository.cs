﻿using DMPAPI.DB.Infrastructure.DataModel;
using DMPAPI.Domain.Repositories.Accounts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Entities = DMPAPI.Domain.Entities;
namespace DMPAPI.DB.Infrastructure.Repositories.Accounts
{
    public class PageRepository : Repository<Domain.Entities.Page>, IPageRepository
    {
        private EntityMapper<Entities.Page> mapperFromDomain = new EntityMapper<Entities.Page>();
        private EntityMapper<DataModel.Page> mapperFromDb = new EntityMapper<DataModel.Page>();

        public PageRepository(DbContext context) : base(context)
        {

        }
        private DMPDbContext dbContext { get { return _dbContext as DMPDbContext; } }

        public int AddPage(Entities.Page page)
        {
            try
            {
                DataModel.Page pageEntity = (DataModel.Page)mapperFromDomain.getMappedObject(page);
                pageEntity.CreatedDate = pageEntity.ModifiedDate = DateTime.Now;
                _dbContext.Set<DataModel.Page>().Add(pageEntity);
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int UpdatePage(Entities.Page page)
        {
            try
            {
                DataModel.Page pageEntity = dbContext.Page.Where(X => X.Id == page.Id).SingleOrDefault();
                if (pageEntity != null)
                {
                    DataModel.Page pageUpdated = (DataModel.Page)mapperFromDomain.getMappedObject(page);
                    pageUpdated.ModifiedDate = DateTime.Now;
                    pageUpdated.CreatedDate = pageUpdated.CreatedDate;
                    dbContext.Entry(pageEntity).CurrentValues.SetValues(pageUpdated);
                    dbContext.Entry(pageEntity).State = EntityState.Modified;
                }
                return 0;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public int DeletePage(Entities.Page page)
        {
            DataModel.Page pageEntity = dbContext.Page.Where(x => x.Id == page.Id).FirstOrDefault();
            if (pageEntity != null)
            {
                dbContext.Page.Remove(pageEntity);
            }
            return 0;
        }


        public List<Entities.Page> GetAllPages()
        {
            List<DataModel.Page> listPAGE = dbContext.Page.ToList();
            List<Entities.Page> listPage = new List<Entities.Page>();

            foreach (DataModel.Page pageEntity in listPAGE)
            {
                listPage.Add((Entities.Page)mapperFromDb.getMappedObject(pageEntity));
            }

            return listPage;
        }

        public Entities.Page GetPageByID(int Id)
        {
            DataModel.Page pageEntity = dbContext.Page.Where(X => X.Id == Id).SingleOrDefault();
            Entities.Page domainPage = (Entities.Page)mapperFromDb.getMappedObject(pageEntity);
            return domainPage;
        }
        public int DeletePagebyID(int Id)
        {
            DataModel.Page pageEntity = dbContext.Page.Where(X => X.Id == Id).SingleOrDefault();
            pageEntity.ModifiedDate = DateTime.Now;
            dbContext.Entry(pageEntity).CurrentValues.SetValues(pageEntity);
            return 0;
        }
    }
}
