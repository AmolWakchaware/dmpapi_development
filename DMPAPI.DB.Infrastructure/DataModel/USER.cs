﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DMPAPI.DB.Infrastructure.DataModel
{
    [Table("Users")]
    public class USER : BaseEntity
    {
        //public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public bool IsPasswordActivated { get; set; }
        public virtual ICollection<Client> userClients { get; set; }
        public virtual ICollection<UserPageRoleMapping> userPageRoleMappings { get; set; }
    }
}
