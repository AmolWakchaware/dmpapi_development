﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.DB.Infrastructure.DataModel
{
    public class Level1 : BaseEntity
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
