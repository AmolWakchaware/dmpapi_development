﻿using DMPAPI.DB.Infrastructure.DataModel.EntityTypeConfiguration;
using System;
using System.Data.Entity;

namespace DMPAPI.DB.Infrastructure.DataModel
{
    public partial class DMPDbContext : DbContext
    {
        public DMPDbContext()
          : base("name=DMPDbContext")
        {
        }

        public DbSet<USER> USERS { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Action> Action { get; set; }
        //public DbSet<RoleAction> RoleAction { get; set; }
        //public DbSet<OpCo> OpCo { get; set; }
        //public DbSet<Project> Project { get; set; }
        //public DbSet<Country> Country { get; set; }
        //public DbSet<Location> Location { get; set; }
        //public DbSet<Experience> Experience { get; set; }
        //public DbSet<Domains> Domain { get; set; }
        //public DbSet<Stream> Stream { get; set; }
        //public DbSet<Skill> Skill { get; set; }
        //public DbSet<Tool> Tool { get; set; }
        //public DbSet<DemandType> DemandType { get; set; }
        //public DbSet<Demand> Demand { get; set; }
        //public DbSet<Panel> Panel { get; set; }
        //public DbSet<Resource> Resource { get; set; }
        //public DbSet<Designation> Designation { get; set; }
        //public DbSet<MapSkillDomain> MapSkillDomain { get; set; }
        //public DbSet<Allocation> Allocation { get; set; }
        //public DbSet<InterviewerType> InterviewerType { get; set; }
        //public DbSet<Status> Status { get; set; }
        public DbSet<Page> Page { get; set; }
       // public DbSet<RolePageMapping> RolePageMapping { get; set; }
        public DbSet<Level1> Level1 { get; set; }
        public DbSet<Level2> Level2 { get; set; }
        public DbSet<Level3> Level3 { get; set; }
        public DbSet<Level4> Level4 { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<UserPageRoleMapping> UserPageRoleMappings { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));

            //Write Fluent API configurations here
            modelBuilder.Configurations.Add(new UserConfiguration());
            //modelBuilder.Configurations.Add(new ActionConfiguration());
            //modelBuilder.Configurations.Add(new RoleConfiguration());
            //modelBuilder.Configurations.Add(new RoleActionConfiguration());
            //modelBuilder.Configurations.Add(new OpCoConfiguration());
            //modelBuilder.Configurations.Add(new ProjectConfiguration());
            //modelBuilder.Configurations.Add(new CountryConfiguration());
            //modelBuilder.Configurations.Add(new LocationConfiguration());
            //modelBuilder.Configurations.Add(new ExperienceConfiguration());
            //modelBuilder.Configurations.Add(new DomainConfiguration());
            //modelBuilder.Configurations.Add(new StreamConfiguration());
            //modelBuilder.Configurations.Add(new SkillConfiguration());
            //modelBuilder.Configurations.Add(new ToolConfiguration());
            //modelBuilder.Configurations.Add(new DemandTypeConfiguration());
            //modelBuilder.Configurations.Add(new PanelConfiguration());
            //modelBuilder.Configurations.Add(new ResourceConfiguration());
            //modelBuilder.Configurations.Add(new DesignationConfiguration());
            //modelBuilder.Configurations.Add(new MapSkillDomainConfiguration());
            //modelBuilder.Configurations.Add(new AllocationConfiguration());
            //modelBuilder.Configurations.Add(new InterviewerTypeConfiguration());
            //modelBuilder.Configurations.Add(new StatusConfiguration());
            //modelBuilder.Configurations.Add(new DemandConfiguration());
            //modelBuilder.Configurations.Add(new PageConfiguration());
            //modelBuilder.Configurations.Add(new RolePageMappingConfiguration());
            //modelBuilder.Configurations.Add(new Level1Configuration());
            //modelBuilder.Configurations.Add(new Level2Configuration());
            //modelBuilder.Configurations.Add(new Level3Configuration());
            //modelBuilder.Configurations.Add(new Level4Configuration());
            //modelBuilder.Configurations.Add(new ClientConfiguration());
           // modelBuilder.Configurations.Add(new UserPageRoleMappingConfiguration());

        }
    }
}
