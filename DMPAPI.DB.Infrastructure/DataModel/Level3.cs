﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.DB.Infrastructure.DataModel
{
    public class Level3 : BaseEntity
    {
        public int Level2Id { get; set; }
        public string Name { get; set; }
        public virtual Level2 Level2 { get; set; }
        public bool IsActive { get; set; }
    }
}
