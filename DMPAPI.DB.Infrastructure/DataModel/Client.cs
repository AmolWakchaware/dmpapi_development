﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.DB.Infrastructure.DataModel
{
    public class Client : BaseEntity
    {
        public string Name { get; set; }
        public int Level4Id { get; set; }
        public virtual Level4 Level4 { get; set; }
        public bool IsActive { get; set; }
    }
}
