﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.DB.Infrastructure.DataModel
{
    public class Level4 : BaseEntity
    {
        public int Level3Id { get; set; }
        public string Name { get; set; }
        public virtual Level3 Level3 { get; set; }
        public bool IsActive { get; set; }
    }
}
