﻿namespace DMPAPI.DB.Infrastructure.DataModel
{
    public class UserPageRoleMapping : BaseEntity
    {
        //public int UserPageRoleMappingId { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public int PageId { get; set; }
        public bool IsRead { get; set; }
        public bool IsWrite { get; set; }
        public bool IsActive { get; set; }
        public virtual USER User { get; set; }
        public virtual Page Page { get; set; }
        public virtual Role Role { get; set; }
    }
}
