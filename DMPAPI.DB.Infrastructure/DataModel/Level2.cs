﻿namespace DMPAPI.DB.Infrastructure.DataModel
{
    public class Level2 : BaseEntity
    {
        public string Name { get; set; }
        public int Level1Id { get; set; }
        public virtual Level1 Level1 { get; set; }
        public bool IsActive { get; set; }
    }
}
