﻿using DMPAPI.Domain.Entities;
using DMPAPI.Domain.Repositories.GenericRepository;

namespace DMPAPI.Domain.Repositories.Accounts
{
    interface IResourceRepository: IRepository<Resource>
    {
    }
}
