﻿using DMPAPI.Domain.Entities;
using DMPAPI.Domain.Repositories.GenericRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.Domain.Repositories.Accounts
{
    public interface IPageRepository : IRepository<Page>
    {
        int AddPage(Page page);
        int UpdatePage(Page page);
        int DeletePage(Page page);
        int DeletePagebyID(int Id);
        List<Page> GetAllPages();
        Page GetPageByID(int Id);
    }
}
