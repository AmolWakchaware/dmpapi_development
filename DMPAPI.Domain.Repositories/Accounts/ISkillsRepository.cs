﻿using DMPAPI.Domain.Entities;
using DMPAPI.Domain.Repositories.GenericRepository;
using System.Collections.Generic;

namespace DMPAPI.Domain.Repositories.Accounts
{
    interface ISkillsRepository : IRepository<Skill>
    {
        int AddSkill(Skill skill);
        int UpdateSkill(Skill skill);
        int DeleteSkill(Skill skill);
        int DeleteSkillbyID(int Id);
        List<Skill> GetAllSkills();
        Skill GetSkillByID(int Id);
    }
}
