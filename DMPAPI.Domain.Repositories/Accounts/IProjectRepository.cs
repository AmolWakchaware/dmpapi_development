﻿using DMPAPI.Domain.Entities;
using DMPAPI.Domain.Repositories.GenericRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.Domain.Repositories.Accounts
{
    interface IProjectRepository : IRepository<Project>
    {
        int AddProject(Project project);
        int UpdateProject(Project project);
        int DeleteProject(Project project);
        int DeleteProjectbyID(int Id);
        List<Project> GetAllProjects();
        Project GetProjectByID(int Id);
    }
}
