﻿using DMPAPI.Domain.Entities;
using DMPAPI.Domain.Repositories.GenericRepository;
using System.Collections.Generic;

namespace DMPAPI.Domain.Repositories.Accounts
{
    public interface IClientRepository : IRepository<Client>
    {
        int AddClient(Client client);
        int UpdateClient(Client client);
        int DeleteClient(Client client);
        int DeleteClientbyID(int Id);
        List<Client> GetAllClient();
        Client GetClientByID(int Id);
        List<Client> GetClientByL4IDs(string Ids);
    }
}
