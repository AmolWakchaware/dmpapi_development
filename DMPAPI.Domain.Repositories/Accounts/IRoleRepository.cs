﻿using DMPAPI.Domain.Entities;
using DMPAPI.Domain.Repositories.GenericRepository;
using System.Collections.Generic;

namespace DMPAPI.Domain.Repositories.Accounts
{
   public interface IRoleRepository : IRepository<Role>
    {
        int AddRole(Role role);
        int UpdateRole(Role role);
        int DeleteRole(Role role);
        int DeleterolebyID(int Id);
        List<Role> GetAllRoles();
        Role GetRoleByID(int Id);
    }
}
