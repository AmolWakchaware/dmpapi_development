﻿using DMPAPI.Domain.Entities;
using DMPAPI.Domain.Repositories.GenericRepository;
using System.Collections.Generic;

namespace DMPAPI.Domain.Repositories.Accounts
{
    public interface IUserRepository: IRepository<User>
    {
        int AddUser(User user);
        int UpdateUser(User user);
        int DeleteUser(User user);
        int DeleteUserbyID(int Id);
        List<User> GetAllUsers();
        User GetUserByID(int Id);
        User GetUserCredentails(string username, string password);
        //string ChangePassword(string opwd, string npwd);
    }
}
