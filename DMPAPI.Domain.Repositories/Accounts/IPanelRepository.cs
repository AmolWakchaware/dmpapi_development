﻿using DMPAPI.Domain.Entities;
using DMPAPI.Domain.Repositories.GenericRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMPAPI.Domain.Repositories.Accounts
{
    interface IPanelRepository : IRepository<Panel>
    {
        int AddPanel(Panel panel);
        int UpdatePanel(Panel panel);
        int DeletePanel(Panel panel);
        int DeletePanelbyID(int Id);
        List<Panel> GetAllPanels();
        Panel GetPanelByID(int Id);
    }
}
