﻿using DMPAPI.Domain.Repositories.Accounts;
using System;

namespace DMPAPI.Domain.Repositories.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository UserDetails { get; }
        //ILocationRepository LocationDetails { get; }
        //ISkillsRepository SkillDetails { get; }
        //IProjectRepository ProjectDetails { get; }
        //IPanelRepository PanelDetails { get; }
        //IOpCoRepository OpCoDetails { get; }
        //IResourceRepository ResourceDetails { get; }
        //IInterviewerTypeRepository InterviewerTypeDetails { get; }
        //ICountryRepository CountryDetails { get; }
        //IDemandRepository DemandDetails { get; }
        IRoleRepository RoleDetails { get; }
        IPageRepository PageDetails { get; }
        //ILevel1Repository Level1Details { get; }
        //ILevel2Repository Level2Details { get; }
        //ILevel3Repository Level3Details { get; }
        //ILevel4Repository Level4Details { get; }
        IClientRepository ClientDetails { get; }
        int SaveChanges();
        int RollbackChanges();
    }
}
