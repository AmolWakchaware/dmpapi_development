﻿using System.Collections.Generic;

namespace DMPAPI.Domain.Repositories.GenericRepository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        void Update(TEntity entity);
        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);


        TEntity GetByID(int Id);
        IEnumerable<TEntity> GetAll();
    }
}
